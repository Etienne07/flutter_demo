import 'package:GreetingsApp/services/auth_service.dart';
import 'package:GreetingsApp/viewmodels/login_view_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class AuthServiceMock extends Mock implements AuthService {}

void main() {
  LoginViewModel sut;
  AuthServiceMock authServiceMock;

  setUp(() {
    authServiceMock = AuthServiceMock();
    sut = LoginViewModel(authServiceMock);
  });

  test("it returns true if authentication is successful", () async {
    when(authServiceMock.authenticate(any, any))
        .thenAnswer((realInvocation) => Future.value(true));

    final result = await sut.authenticate("admin", "admin");

    expect(result, true);
  });

  test("it throws if username is less than 5 characters", () async {
    expect(
        sut.authenticate("adm", "admin"),
        throwsA(isA<Exception>().having((error) => error.toString(),
            "Check exception", "Exception: Validation failed")));
  });

  test("it throws if authentication is not successful", () async {
    when(authServiceMock.authenticate(any, any)).thenThrow(Exception("error"));

    expect(
        sut.authenticate("admin", "admin"),
        throwsA(isA<Exception>().having((error) => error.toString(),
            "Check exception", "Exception: error")));
  });
}
