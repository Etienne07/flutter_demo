import 'package:GreetingsApp/viewmodels/cart_list_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CartListWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ChangeNotifierProvider(
        create: (_) => CartListViewModel(),
        child: Consumer<CartListViewModel>(
          builder: (BuildContext context, viewModel, Widget child) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Text(
                    "Total number of items: ${viewModel.totalNumberOfItems}",
                    style: TextStyle(fontSize: 22.0),
                  ),
                  SizedBox(height: 32.0,),
                  Expanded(
                    child: ListView.separated(
                      itemBuilder: (context, index) =>
                          Item(viewModel.items[index]),
                      separatorBuilder: (context, index) => Divider(),
                      itemCount: viewModel.items.length,
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}

class Item extends StatelessWidget {
  String name;

  Item(this.name);

  void _removeItem(BuildContext context, String name) {
    Provider.of<CartListViewModel>(context, listen: false).removeItem(name);
    Scaffold.of(context)
        .showSnackBar(SnackBar(content: Text("$name dismissed")));
  }

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: Key(name),
      child: SizedBox(
        child: Align(
          child: Text(
            name,
            style: TextStyle(fontSize: 22.0),
          ),
          alignment: Alignment.centerLeft,
        ),
        height: 32.0,
      ),
      onDismissed: (_) => _removeItem(context, name),
    );
  }
}
