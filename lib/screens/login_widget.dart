import 'package:flutter/material.dart';

import '../viewmodels/login_view_model.dart';
import '../services/auth_service.dart';

class LoginWidget extends StatefulWidget {
  @override
  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget> {
  LoginViewModel _loginViewModel = LoginViewModel(AuthService());

  TextEditingController usernameTextController = new TextEditingController();
  TextEditingController passwordTextController = new TextEditingController();
  var errorMessage = "";

  void _onLoginButtonPressed() {
    _loginViewModel
        .authenticate(usernameTextController.text, passwordTextController.text)
        .then((userProfile) {
      Navigator.of(context).pushNamed('/cart-list');
    }).catchError((error) {
      setState(() {
        errorMessage = error.toString();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Login"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(children: [
          TextField(
            decoration: const InputDecoration(hintText: "Username"),
            controller: usernameTextController,
          ),
          TextField(
            decoration: const InputDecoration(hintText: "Password"),
            controller: passwordTextController,
          ),
          RaisedButton(
            onPressed: _onLoginButtonPressed,
            child: const Text('Login'),
          ),
          Text(errorMessage)
        ]),
      ),
    );
  }
}
