import 'package:flutter/material.dart';

class CartListViewModel extends ChangeNotifier {
  List<String> _items;
  int _totalNumberOfItems;

  CartListViewModel() {
    _items = ["Banana", "Apple", "Apricot", "Pear", "Orange"];
    _totalNumberOfItems = _items.length;
  }

  List<String> get items => _items;

  int get totalNumberOfItems => _totalNumberOfItems;

  removeItem(item) {
    this._items.remove(item);
    this._totalNumberOfItems = _items.length;
    notifyListeners();
  }
}
