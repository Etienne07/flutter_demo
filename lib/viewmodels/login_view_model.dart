import 'package:GreetingsApp/services/auth_service.dart';

class LoginViewModel {
  AuthService _authService;

  LoginViewModel(this._authService);

  Future<bool> authenticate(String username, String password) async {
    if (username.length >= 5 && password.length >= 5) {
      return await this._authService.authenticate(username, password);
    } else {
      throw Exception("Validation failed");
    }
  }


}
