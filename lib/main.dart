import 'package:GreetingsApp/screens/cart_list_widget.dart';
import 'package:flutter/material.dart';

import 'screens/login_widget.dart';

void main() => runApp(
      MaterialApp(
        routes: {
          '/': (context) => LoginWidget(),
          '/cart-list': (context) => CartListWidget(),
        },
      ),
    );
